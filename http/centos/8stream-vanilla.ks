#install # deprecated
text
keyboard us
lang en_US.UTF-8
skipx
network --device eth0 --bootproto dhcp
rootpw --plaintext test
user --name=rvn --password=rvn
sshkey --username rvn "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3e0frRD6el0aK+kh6Ba4/Mc3pYs99BBVWf8pw3yMO31wR6+l/uscSu15oJGxbfsZknqZKKVfD925q03wcblzA3z0Q9F6uPb+gK+/q8+xWU9yzDKNF7zjqdzVuP+6E2xQv48pfAxu1/m3bqQHL1jLIbJG2V+53WaaCx+RG8OmyxPH0MVHzX2ajtpDP1K6/fKr+wCefxJ2yo6WRRh/95KlhaxLNPNql3xJno3HIYd3x2+hR0hajtPHM6Gj2LORVUXui8M0ZaO/NWeyy3mcK3KTk2rkKlztuIFUyp7Z5L+wPzp6MEgDaQjoOEZXEM7WwqbeSNYv/2pQlOlZcTyYhRuTR ry@mirror"
firewall --disabled
selinux --disabled
timezone --utc America/Los_Angeles --ntpservers north-america.pool.ntp.org
services --enabled=sshd
# The biosdevname and ifnames options ensure we get "eth0" as our interface
# even in environments like virtualbox that emulate a real NW card
bootloader --append="console=ttyS0,115200n8 console=tty0 net.ifnames=0 biosdevname=0"
zerombr
clearpart --all --drives=vda
part / --fstype=ext4 --asprimary --size=1024 --grow --label root --ondisk=vda
part swap --size=1024 --label swap
url --mirrorlist http://mirrorlist.centos.org/?release=8-stream&arch=$basearch&repo=BaseOS
repo --name baseos --mirrorlist http://mirrorlist.centos.org/?release=8-stream&arch=$basearch&repo=BaseOS&infra=$infra
repo --name appstream --mirrorlist http://mirrorlist.centos.org/?release=8-stream&arch=$basearch&repo=AppStream&infra=$infra
repo --name extras --mirrorlist http://mirrorlist.centos.org/?release=8-stream&arch=$basearch&repo=extras&infra=$infra
repo --name epel --mirrorlist https://mirrors.fedoraproject.org/mirrorlist?repo=epel-8&arch=$basearch

reboot

%packages --instLangs=en
@core
#epel-release
# includes the lsb_release command
redhat-lsb-core
drpm
bash-completion
man-pages
bzip2
rsync
yum-utils
net-tools
# from raven customizations, previously in dnf.sh
vim-minimal
emacs-nox
tree
tmux
nfs-utils
python3-libselinux
# experiments usually don't use graphics no need for Plymouth
-plymouth
# Don't build rescue initramfs (unless you need to debug on a node)
-dracut-config-rescue
%end


%post

# Fix for https://github.com/CentOS/sig-cloud-instance-build/issues/38
cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DEVICE="eth0"
BOOTPROTO="dhcp"
ONBOOT="yes"
TYPE="Ethernet"
PERSISTENT_DHCLIENT="yes"
EOF

# sshd: disable DNS checks
ex -s /etc/ssh/sshd_config <<EOF
:%substitute/^#\(UseDNS\) yes$/&\r\1 no/
:update
:quit
EOF

# rvn sudoers
cat > /etc/sudoers.d/root-group <<EOF
%root ALL=(ALL) NOPASSWD: ALL
%rvn ALL=(ALL) NOPASSWD: ALL
EOF

# adjust fsck's to zero for /
uuid=$(cat /proc/cmdline | grep -Po 'root=UUID=\K([0-9a-f\-]+)')
tune2fs -c 0 "/dev/disk/by-uuid/$uuid"

# systemd should generate a new machine id during the first boot.
# /etc/machine-id should be empty, but it must exist to prevent
# boot errors (e.g.  systemd-journald failing to start).
:>/etc/machine-id
:>/var/lib/dbus/machine-id

# Blacklist the floppy module to avoid probing timeouts
echo blacklist floppy > /etc/modprobe.d/nofloppy.conf
#chcon -u system_u -r object_r -t modules_conf_t /etc/modprobe.d/nofloppy.conf

# Customize the initramfs
pushd /etc/dracut.conf.d
# There's no floppy controller, but probing for it generates timeouts
echo 'omit_drivers+=" floppy "' > nofloppy.conf
popd

# The following step is unlikely to be necessary during %post.
# Rerun dracut for the installed kernel (not the running kernel):
#KERNEL_VERSION=$(rpm -q kernel --qf '%{version}-%{release}.%{arch}\n')
#dracut -fv /boot/initramfs-$(uname -r).img $(uname -r)

# Seal for deployment
#rm -rf /etc/ssh/ssh_host_*
#hostnamectl set-hostname localhost.localdomain
#rm -rf /etc/udev/rules.d/70-*

# clean up package stuff
dnf autoremove -y
dnf clean all -y
%end
