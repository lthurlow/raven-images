sudo sed \
  -ie \
  's/GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="net.ifnames=0 biosdevname=0 earlyprintk=ttyS1,9600 console=ttyS1,9600 loglevel=7 tsc=reliable"/g' \
  /etc/default/grub

sudo sed \
  -ie \
  "s/ens4/eth0/g" \
  /etc/network/interfaces

sudo update-grub

