#!/bin/bash

sudo apt-get update
sudo apt-get install gnupg2 curl
curl -L https://pkg.mergetb.net/addrepo | RELEASE=kass bash -

sudo apt install -y linux-image-5.5.0-rc6-moa+ libbpf-moa linux-headers-5.5.0-rc6-moa+

entry0=`cat /boot/grub/grub.cfg | grep menuentry | grep gnulinux-advanced | grep -v recovery | egrep -o 'gnulinux-advanced-[0-9a-f-]*'`
entry1=`cat /boot/grub/grub.cfg | grep menuentry | grep 5.5.0-rc6-moa | grep -v recovery | egrep -o 'gnulinux-5\.5\.0-rc6-moa\+-advanced-[0-9a-f-]*'`
entry="$entry0>$entry1"
sudo sed -i "s/GRUB_DEFAULT=0/GRUB_DEFAULT=\"$entry0>$entry1\"/" /etc/default/grub
sudo update-grub2

cat >/tmp/resume << EOF
RESUME=none
EOF
sudo cp /tmp/resume /etc/initramfs-tools/conf.d/resume
sudo update-initramfs -u
