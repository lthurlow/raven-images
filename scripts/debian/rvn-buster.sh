#!/bin/bash

set -e
set -x

export DEBIAN_FRONTEND=noninteractive

sudo apt-get install -y snapd
