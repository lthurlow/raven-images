#!/bin/bash

sudo apt-get install gnupg
curl -L https://pkg.mergetb.net/gpg | sudo apt-key add -


cat >/tmp/merge.list << EOF
deb [arch=amd64] https://pkg.mergetb.net/mergetb-next buster main
EOF

sudo cp /tmp/merge.list /etc/apt/sources.list.d/merge.list

sudo apt-get update
sudo apt-get install linux-image-4.19.0-td+

entry0=`cat /boot/grub/grub.cfg | grep menuentry | grep gnulinux-advanced | grep -v recovery | egrep -o 'gnulinux-advanced-[0-9a-f-]*'`
entry1=`cat /boot/grub/grub.cfg | grep menuentry | grep 4.19.0-td | grep -v recovery | egrep -o 'gnulinux-4\.19\.0-td\+-advanced-[0-9a-f-]*'`
entry="$entry0>$entry1"
sudo sed -i "s/GRUB_DEFAULT=0/GRUB_DEFAULT=\"$entry0>$entry1\"/" /etc/default/grub
sudo update-grub2

cat >/tmp/resume << EOF
RESUME=none
EOF
sudo cp /tmp/resume /etc/initramfs-tools/conf.d/resume
sudo update-initramfs -u
