#!/bin/bash

set -e
set -x

sudo apt-get clean
sudo rm /etc/discover-pkginstall.conf
sudo rm -f /etc/machine-id
sudo rm -f /var/lib/dbus/machine-id

cat > /tmp/machine-id-init.service << EOF
[Unit]
Description=Initialize machine id

[Service]
Type=oneshot
ExecStart=/bin/systemd-machine-id-setup

[Install]
WantedBy=multi-user.target
EOF
sudo mv /tmp/machine-id-init.service /etc/systemd/system/machine-id-init.service

sudo systemctl daemon-reload
sudo systemctl enable machine-id-init.service
