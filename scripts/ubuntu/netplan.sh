#!/bin/bash

rm /etc/netplan/*

cat << EOF >> /etc/netplan/config.yaml
---
network:
  version: 2
  ethernets:
    eth0:
      dhcp4: true
      dhcp-identifier: mac
EOF

