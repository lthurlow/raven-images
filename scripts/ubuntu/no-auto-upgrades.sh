#!/bin/bash

cat <<EOF > /tmp/aa
APT::Periodic::Update-Package-Lists "0";
APT::Periodic::Unattended-Upgrade "0";
EOF

sudo cp /tmp/aa /etc/apt/apt.conf.d/20auto-upgrades
