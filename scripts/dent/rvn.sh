#!/bin/bash

groupadd rvn
useradd rvn -s /bin/bash -m -g rvn -G sudo
echo rvn:rvn | sudo chpasswd
mkdir -p /home/rvn/.ssh
wget -O /home/rvn/.ssh/authorized_keys --no-check-certificate https://mirror.deterlab.net/rvn/rvn.pub
chmod 700 /home/rvn/.ssh/
chmod 600 /home/rvn/.ssh/authorized_keys
chown -R rvn:rvn /home/rvn
echo '%rvn ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/root-group
