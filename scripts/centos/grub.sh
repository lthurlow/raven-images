#!/bin/bash

set -e

# troubleshooting/debugging output
cat /etc/default/grub

# just adding 2 lines to the end to extend default function
# rather than overwite everything when they are mostly the same.
# the emulab/deter way removed a few lines that may be necessary
# for normal operations going forward with changes to grubby (etc)
# such as GRUB_ENABLE_BLSCFG
cat >> /etc/default/grub <<EOF
GRUB_TERMINAL="console serial"
GRUB_SERIAL_COMMAND="serial --unit=0 --speed=115200 --word=8 --parity=no --stop=1"
EOF

# Make sure to install to a partition in case Emulab/DETER is still nuking MBR
grub2-mkconfig -o /boot/grub2/grub.cfg
# EFI systems would have this file here
[[ -e /boot/efi/EFI/centos/grub.cfg ]] && grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg

uuid=$(cat /proc/cmdline | grep -Po 'root=UUID=\K([0-9a-f\-]+)')
grub2-install --force "/dev/disk/by-uuid/$uuid"
