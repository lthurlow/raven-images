#!/bin/bash

set -e
set -x

# don't need to do these right now, since all packages
#  are installed during kickstart for centos8stream
#dnf autoremove -y
#dnf clean all -y

# this should be done to keep live machine-id's unique
echo "clearing machine-id files /etc/machine-id, /var/lib/dbus/machine-id"
:>/etc/machine-id
:>/var/lib/dbus/machine-id

echo "clearing out network manager resolvconf"
cat > /etc/resolv.conf <<EOF
nameserver 8.8.8.8
EOF
