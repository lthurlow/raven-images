#!/usr/bin/env bash

# just waiting a long time for debugging/troubleshooting
# at the end of a build (allows you to ssh in and look around)

sleep 6000
